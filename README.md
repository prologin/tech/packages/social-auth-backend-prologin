# social-auth-backend-prologin

This is Prologin's python social auth backend.

## How to use with django

1. Add this line to your requirements.txt

```
social-auth-backend-prologin==0.1.3
```

2. Add these in your settings.py

```python

# add the app to installed apps

INSTALLED_APPS = [
    ...
    "social_django",
    ...
]

# if you are not sure what to put here, see with a prologin root
SOCIAL_AUTH_PROLOGIN_SCOPE = [
    "email",
    "profile",
    "contest",
    "security_clearance",
]

# same thing

SOCIAL_AUTH_PIPELINE = (
    "social_core.pipeline.social_auth.social_details",
    "social_core.pipeline.social_auth.social_uid",
    "social_core.pipeline.social_auth.auth_allowed",
    "social_core.pipeline.social_auth.social_user",
    "social_core.pipeline.user.get_username",
    "social_core.pipeline.user.create_user",
    "social_core.pipeline.social_auth.associate_user",
    "social_core.pipeline.social_auth.load_extra_data",
    "social_auth_backend_prologin.pipeline.save_all_claims_as_extra_data",
    "social_core.pipeline.user.user_details",
    "social_auth_backend_prologin.pipeline.apply_upstream_security_clearances",
)

# if you want OIDC-only authentication you may remove the django... line

AUTHENTICATION_BACKENDS = (
    "social_auth_backend_prologin.backend.ProloginOpenIdConnect",
    "django.contrib.auth.backends.ModelBackend",
)

# These one are secret credentials, please do not commit them
# (They are given by prologin roots)

SOCIAL_AUTH_PROLOGIN_KEY = (
    "CHANGE_ME"  # This is the client ID given by a Prologin Root
)

SOCIAL_AUTH_PROLOGIN_SECRET = (
    "CHANGE_ME"  # This is the client secret given by a Prologin Root
)

```

3. add these in your `urls.py`

```

urlpatterns = [
    ...
    path('sso/', include('social_django.urls', namespace='social'),
    ...
]

```
