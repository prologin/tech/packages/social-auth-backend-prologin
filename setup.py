from setuptools import setup

setup(
    install_requires=[
        'social-auth-app-django==4.0.0',
        'python-jose==3.3.0',
    ],
)
